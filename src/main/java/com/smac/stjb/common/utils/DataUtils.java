/*
 * %W% %E% smac_training_datpt - Meii
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.smac.stjb.common.utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Class description goes here.
 * 
 * @since 4:44:38 PM
 * @author datpt
 */
public class DataUtils {

	/**
	 * Check null or empty su dung mang nguon cua thu vien String Utils
	 * @param cs String
	 * @return Boolean
	 */
	public static boolean isNullOrEmpty(CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}

		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(cs.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check null or empty cho Collection
	 * @param collection
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(final Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	/**
	 * Check null or empty cho Object
	 * @param collection
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(final Object[] collection) {
		return collection == null || collection.length == 0;
	}

	/**
	 * Check null or empty cho map
	 * @param map
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(final Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

	
	/**
	 * Check null cho tat ca cac kieu du lieu
	 * @param obj
	 * @return boolean
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isNullObject(Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof String) {
			return isNullOrEmpty(obj.toString());
		}
		if (obj instanceof Long) {
			if (obj.equals(0L)) {
				return true;
			}
		}
		if (obj instanceof List) {
			return isNullOrEmpty((Collection)obj);
		}
		return false;
	}
	
}

