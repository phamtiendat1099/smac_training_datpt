/*
 * %W% %E% smac_training_datpt - Meii
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.smac.stjb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.smac.stjb.business.AppParamsBusiness;
import com.smac.stjb.database.entity.AppParams;

/**
 * Class description goes here.
 * 
 * @since		10:21:25 AM
 * @author		Meii
 */

@RestController
public class AppParamsServiceImpl {

	@Autowired
	private AppParamsBusiness appParamsBusiness;
	
	@RequestMapping("/getListAppParamsAvailable")
	public List<AppParams> getListAppParamsAvailable(){
		return appParamsBusiness.getListAppParamsAvailable();
	}
	
	
	@RequestMapping("/getListAppParamsByTypeAndCode")
	public List<AppParams> getListAppParamsByTypeAndCode(
			@RequestParam(name = "type", required = false) String type,
			@RequestParam(name = "code", required = false) String code) {
		return appParamsBusiness.getListAppParamsByTypeAndCode(type, code);
	}
}
