package com.smac.stjb.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories("com.smac.stjb.database.*")
@EntityScan("com.smac.stjb.database.*") 
@ComponentScan(basePackages = "com.smac.*")

public class SmacTrainingDatptApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmacTrainingDatptApplication.class, args);
	}

}
