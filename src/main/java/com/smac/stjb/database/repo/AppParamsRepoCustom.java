/*
 * %W% %E% smac_training_datpt - Meii
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.smac.stjb.database.repo;

import java.util.List;

import com.smac.stjb.database.entity.AppParams;

/**
 * Class description goes here.
 * 
 * @since		11:50:43 AM
 * @author		Meii
 */
public interface AppParamsRepoCustom {
	
	/**
	 * Ham nay dung de lay ra danh sach AppParams theo type va code
	 * @param type: tham so truyen vao kieu String
	 * @param code: tham so truyen vao kieu String
	 * @return : Tra ve 1 list AppParams
	 */
	public List<AppParams> getListAppParamsByTypeAndCode(String type, String code);

}
