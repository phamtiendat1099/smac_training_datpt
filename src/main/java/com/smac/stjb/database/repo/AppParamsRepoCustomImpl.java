/*
 * %W% %E% smac_training_datpt - Meii
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.smac.stjb.database.repo;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.smac.stjb.common.utils.DataUtils;
import com.smac.stjb.database.entity.AppParams;

/**
 * Class description goes here.
 * 
 * @since		11:52:01 AM
 * @author		Meii
 */

@SuppressWarnings("unchecked")
public class AppParamsRepoCustomImpl implements AppParamsRepoCustom{

	@PersistenceContext
	private EntityManager em;	
	
	@SuppressWarnings("static-access")
	@Override
	public List<AppParams> getListAppParamsByTypeAndCode(String type, String code) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select c from AppParams c ");
		if (!DataUtils.isNullObject(type)) {
			sql.append("where c.appParamsPk.type = :type ");
		}
		if (!DataUtils.isNullObject(code)) {
			if (!DataUtils.isNullObject(type)) sql.append("and c.appParamsPk.code = :code");
			else sql.append("where c.appParamsPk.code = :code ");
		}	
		Query query = em.createQuery(sql.toString());
		if (!DataUtils.isNullObject(type)) query.setParameter("type", type);
		if (!DataUtils.isNullObject(code)) query.setParameter("code", code);
		
		return query.getResultList();
	}

}
