/*
 * %W% %E% smac_training_datpt - Meii
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.smac.stjb.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.smac.stjb.business.AppParamsBusiness;
import com.smac.stjb.common.utils.Constant;
import com.smac.stjb.database.entity.AppParams;
import com.smac.stjb.database.repo.AppParamsRepo;

/**
 * Class description goes here.
 * 
 * @since		11:43:38 AM
 * @author		Meii
 */

@Component
public class AppParamsBusinessImpl implements AppParamsBusiness{
	
	@Autowired
	AppParamsRepo appParamsRepo;

	@Override
	public List<AppParams> getListAppParamsAvailable() {
		return appParamsRepo.findByStatus(Constant.STATUS.ACTIVE);
	}

	@Override
	public List<AppParams> getListAppParamsByTypeAndCode(String type, String code) {
		return appParamsRepo.getListAppParamsByTypeAndCode(type, code);
	}

}
