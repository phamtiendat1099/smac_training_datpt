create table app_params(
    type varchar2(50) not null,
    code varchar2(500) not null,
    name varchar2(500) not null,
    status varchar2(1) not null,
    value varchar2(100)
); 